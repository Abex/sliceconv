package sliceconv
import (
	"reflect"
	"unsafe"
)

// Uint32ToUint16 does a unsafe cast from 'uint32' to 'uint16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint32ToUint16 (i []uint32) []uint16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Uint32ToByte does a unsafe cast from 'uint32' to 'byte.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint32ToByte (i []uint32) []byte {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]byte, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Uint32ToFloat32 does a unsafe cast from 'uint32' to 'float32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint32ToFloat32 (i []uint32) []float32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Uint32ToInt32 does a unsafe cast from 'uint32' to 'int32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint32ToInt32 (i []uint32) []int32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Uint32ToInt64 does a unsafe cast from 'uint32' to 'int64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint32ToInt64 (i []uint32) []int64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Uint32ToFloat64 does a unsafe cast from 'uint32' to 'float64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint32ToFloat64 (i []uint32) []float64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Uint32ToUint8 does a unsafe cast from 'uint32' to 'uint8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint32ToUint8 (i []uint32) []uint8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Uint32ToUint64 does a unsafe cast from 'uint32' to 'uint64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint32ToUint64 (i []uint32) []uint64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Uint32ToInt8 does a unsafe cast from 'uint32' to 'int8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint32ToInt8 (i []uint32) []int8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Uint32ToInt16 does a unsafe cast from 'uint32' to 'int16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint32ToInt16 (i []uint32) []int16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Uint32ToString does a unsafe cast from 'uint32' to 'string.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint32ToString (i []uint32) []string {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]string, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Uint64ToFloat32 does a unsafe cast from 'uint64' to 'float32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint64ToFloat32 (i []uint64) []float32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Uint64ToUint16 does a unsafe cast from 'uint64' to 'uint16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint64ToUint16 (i []uint64) []uint16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Uint64ToByte does a unsafe cast from 'uint64' to 'byte.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint64ToByte (i []uint64) []byte {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]byte, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 8
  rethead.Len = ihead.Len * 8
  return ret
}

// Uint64ToInt8 does a unsafe cast from 'uint64' to 'int8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint64ToInt8 (i []uint64) []int8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 8
  rethead.Len = ihead.Len * 8
  return ret
}

// Uint64ToInt16 does a unsafe cast from 'uint64' to 'int16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint64ToInt16 (i []uint64) []int16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Uint64ToInt32 does a unsafe cast from 'uint64' to 'int32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint64ToInt32 (i []uint64) []int32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Uint64ToInt64 does a unsafe cast from 'uint64' to 'int64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint64ToInt64 (i []uint64) []int64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Uint64ToFloat64 does a unsafe cast from 'uint64' to 'float64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint64ToFloat64 (i []uint64) []float64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Uint64ToUint8 does a unsafe cast from 'uint64' to 'uint8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint64ToUint8 (i []uint64) []uint8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 8
  rethead.Len = ihead.Len * 8
  return ret
}

// Uint64ToUint32 does a unsafe cast from 'uint64' to 'uint32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint64ToUint32 (i []uint64) []uint32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Uint64ToString does a unsafe cast from 'uint64' to 'string.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint64ToString (i []uint64) []string {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]string, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 8
  rethead.Len = ihead.Len * 8
  return ret
}

// Int8ToUint64 does a unsafe cast from 'int8' to 'uint64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int8ToUint64 (i []int8) []uint64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 8
  rethead.Len = ihead.Len / 8
  return ret
}

// Int8ToInt16 does a unsafe cast from 'int8' to 'int16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int8ToInt16 (i []int8) []int16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Int8ToInt32 does a unsafe cast from 'int8' to 'int32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int8ToInt32 (i []int8) []int32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// Int8ToInt64 does a unsafe cast from 'int8' to 'int64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int8ToInt64 (i []int8) []int64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 8
  rethead.Len = ihead.Len / 8
  return ret
}

// Int8ToFloat64 does a unsafe cast from 'int8' to 'float64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int8ToFloat64 (i []int8) []float64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 8
  rethead.Len = ihead.Len / 8
  return ret
}

// Int8ToUint8 does a unsafe cast from 'int8' to 'uint8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int8ToUint8 (i []int8) []uint8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Int8ToUint32 does a unsafe cast from 'int8' to 'uint32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int8ToUint32 (i []int8) []uint32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// Int8ToString does a unsafe cast from 'int8' to 'string.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int8ToString (i []int8) []string {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]string, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Int8ToFloat32 does a unsafe cast from 'int8' to 'float32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int8ToFloat32 (i []int8) []float32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// Int8ToUint16 does a unsafe cast from 'int8' to 'uint16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int8ToUint16 (i []int8) []uint16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Int8ToByte does a unsafe cast from 'int8' to 'byte.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int8ToByte (i []int8) []byte {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]byte, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Int16ToUint8 does a unsafe cast from 'int16' to 'uint8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int16ToUint8 (i []int16) []uint8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Int16ToUint32 does a unsafe cast from 'int16' to 'uint32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int16ToUint32 (i []int16) []uint32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Int16ToUint64 does a unsafe cast from 'int16' to 'uint64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int16ToUint64 (i []int16) []uint64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// Int16ToInt8 does a unsafe cast from 'int16' to 'int8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int16ToInt8 (i []int16) []int8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Int16ToInt32 does a unsafe cast from 'int16' to 'int32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int16ToInt32 (i []int16) []int32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Int16ToInt64 does a unsafe cast from 'int16' to 'int64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int16ToInt64 (i []int16) []int64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// Int16ToFloat64 does a unsafe cast from 'int16' to 'float64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int16ToFloat64 (i []int16) []float64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// Int16ToString does a unsafe cast from 'int16' to 'string.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int16ToString (i []int16) []string {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]string, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Int16ToUint16 does a unsafe cast from 'int16' to 'uint16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int16ToUint16 (i []int16) []uint16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Int16ToByte does a unsafe cast from 'int16' to 'byte.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int16ToByte (i []int16) []byte {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]byte, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Int16ToFloat32 does a unsafe cast from 'int16' to 'float32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int16ToFloat32 (i []int16) []float32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Int32ToUint8 does a unsafe cast from 'int32' to 'uint8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int32ToUint8 (i []int32) []uint8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Int32ToUint32 does a unsafe cast from 'int32' to 'uint32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int32ToUint32 (i []int32) []uint32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Int32ToUint64 does a unsafe cast from 'int32' to 'uint64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int32ToUint64 (i []int32) []uint64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Int32ToInt8 does a unsafe cast from 'int32' to 'int8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int32ToInt8 (i []int32) []int8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Int32ToInt16 does a unsafe cast from 'int32' to 'int16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int32ToInt16 (i []int32) []int16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Int32ToInt64 does a unsafe cast from 'int32' to 'int64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int32ToInt64 (i []int32) []int64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Int32ToFloat64 does a unsafe cast from 'int32' to 'float64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int32ToFloat64 (i []int32) []float64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Int32ToString does a unsafe cast from 'int32' to 'string.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int32ToString (i []int32) []string {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]string, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Int32ToUint16 does a unsafe cast from 'int32' to 'uint16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int32ToUint16 (i []int32) []uint16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Int32ToByte does a unsafe cast from 'int32' to 'byte.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int32ToByte (i []int32) []byte {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]byte, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Int32ToFloat32 does a unsafe cast from 'int32' to 'float32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int32ToFloat32 (i []int32) []float32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Int64ToUint16 does a unsafe cast from 'int64' to 'uint16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int64ToUint16 (i []int64) []uint16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Int64ToByte does a unsafe cast from 'int64' to 'byte.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int64ToByte (i []int64) []byte {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]byte, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 8
  rethead.Len = ihead.Len * 8
  return ret
}

// Int64ToFloat32 does a unsafe cast from 'int64' to 'float32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int64ToFloat32 (i []int64) []float32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Int64ToUint8 does a unsafe cast from 'int64' to 'uint8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int64ToUint8 (i []int64) []uint8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 8
  rethead.Len = ihead.Len * 8
  return ret
}

// Int64ToUint32 does a unsafe cast from 'int64' to 'uint32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int64ToUint32 (i []int64) []uint32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Int64ToUint64 does a unsafe cast from 'int64' to 'uint64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int64ToUint64 (i []int64) []uint64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Int64ToInt8 does a unsafe cast from 'int64' to 'int8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int64ToInt8 (i []int64) []int8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 8
  rethead.Len = ihead.Len * 8
  return ret
}

// Int64ToInt16 does a unsafe cast from 'int64' to 'int16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int64ToInt16 (i []int64) []int16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Int64ToInt32 does a unsafe cast from 'int64' to 'int32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int64ToInt32 (i []int64) []int32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Int64ToFloat64 does a unsafe cast from 'int64' to 'float64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int64ToFloat64 (i []int64) []float64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Int64ToString does a unsafe cast from 'int64' to 'string.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Int64ToString (i []int64) []string {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]string, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 8
  rethead.Len = ihead.Len * 8
  return ret
}

// Float64ToUint16 does a unsafe cast from 'float64' to 'uint16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float64ToUint16 (i []float64) []uint16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Float64ToByte does a unsafe cast from 'float64' to 'byte.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float64ToByte (i []float64) []byte {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]byte, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 8
  rethead.Len = ihead.Len * 8
  return ret
}

// Float64ToFloat32 does a unsafe cast from 'float64' to 'float32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float64ToFloat32 (i []float64) []float32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Float64ToInt64 does a unsafe cast from 'float64' to 'int64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float64ToInt64 (i []float64) []int64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Float64ToUint8 does a unsafe cast from 'float64' to 'uint8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float64ToUint8 (i []float64) []uint8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 8
  rethead.Len = ihead.Len * 8
  return ret
}

// Float64ToUint32 does a unsafe cast from 'float64' to 'uint32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float64ToUint32 (i []float64) []uint32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Float64ToUint64 does a unsafe cast from 'float64' to 'uint64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float64ToUint64 (i []float64) []uint64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Float64ToInt8 does a unsafe cast from 'float64' to 'int8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float64ToInt8 (i []float64) []int8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 8
  rethead.Len = ihead.Len * 8
  return ret
}

// Float64ToInt16 does a unsafe cast from 'float64' to 'int16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float64ToInt16 (i []float64) []int16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Float64ToInt32 does a unsafe cast from 'float64' to 'int32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float64ToInt32 (i []float64) []int32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Float64ToString does a unsafe cast from 'float64' to 'string.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float64ToString (i []float64) []string {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]string, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 8
  rethead.Len = ihead.Len * 8
  return ret
}

// Uint8ToFloat32 does a unsafe cast from 'uint8' to 'float32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint8ToFloat32 (i []uint8) []float32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// Uint8ToUint16 does a unsafe cast from 'uint8' to 'uint16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint8ToUint16 (i []uint8) []uint16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Uint8ToByte does a unsafe cast from 'uint8' to 'byte.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint8ToByte (i []uint8) []byte {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]byte, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Uint8ToUint64 does a unsafe cast from 'uint8' to 'uint64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint8ToUint64 (i []uint8) []uint64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 8
  rethead.Len = ihead.Len / 8
  return ret
}

// Uint8ToInt8 does a unsafe cast from 'uint8' to 'int8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint8ToInt8 (i []uint8) []int8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Uint8ToInt16 does a unsafe cast from 'uint8' to 'int16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint8ToInt16 (i []uint8) []int16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Uint8ToInt32 does a unsafe cast from 'uint8' to 'int32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint8ToInt32 (i []uint8) []int32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// Uint8ToInt64 does a unsafe cast from 'uint8' to 'int64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint8ToInt64 (i []uint8) []int64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 8
  rethead.Len = ihead.Len / 8
  return ret
}

// Uint8ToFloat64 does a unsafe cast from 'uint8' to 'float64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint8ToFloat64 (i []uint8) []float64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 8
  rethead.Len = ihead.Len / 8
  return ret
}

// Uint8ToUint32 does a unsafe cast from 'uint8' to 'uint32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint8ToUint32 (i []uint8) []uint32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// Uint8ToString does a unsafe cast from 'uint8' to 'string.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint8ToString (i []uint8) []string {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]string, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// StringToUint32 does a unsafe cast from 'string' to 'uint32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func StringToUint32 (i []string) []uint32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// StringToUint64 does a unsafe cast from 'string' to 'uint64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func StringToUint64 (i []string) []uint64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 8
  rethead.Len = ihead.Len / 8
  return ret
}

// StringToInt8 does a unsafe cast from 'string' to 'int8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func StringToInt8 (i []string) []int8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// StringToInt16 does a unsafe cast from 'string' to 'int16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func StringToInt16 (i []string) []int16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// StringToInt32 does a unsafe cast from 'string' to 'int32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func StringToInt32 (i []string) []int32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// StringToInt64 does a unsafe cast from 'string' to 'int64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func StringToInt64 (i []string) []int64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 8
  rethead.Len = ihead.Len / 8
  return ret
}

// StringToFloat64 does a unsafe cast from 'string' to 'float64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func StringToFloat64 (i []string) []float64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 8
  rethead.Len = ihead.Len / 8
  return ret
}

// StringToUint8 does a unsafe cast from 'string' to 'uint8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func StringToUint8 (i []string) []uint8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// StringToByte does a unsafe cast from 'string' to 'byte.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func StringToByte (i []string) []byte {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]byte, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// StringToFloat32 does a unsafe cast from 'string' to 'float32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func StringToFloat32 (i []string) []float32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// StringToUint16 does a unsafe cast from 'string' to 'uint16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func StringToUint16 (i []string) []uint16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// ByteToUint16 does a unsafe cast from 'byte' to 'uint16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func ByteToUint16 (i []byte) []uint16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// ByteToFloat32 does a unsafe cast from 'byte' to 'float32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func ByteToFloat32 (i []byte) []float32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// ByteToInt16 does a unsafe cast from 'byte' to 'int16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func ByteToInt16 (i []byte) []int16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// ByteToInt32 does a unsafe cast from 'byte' to 'int32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func ByteToInt32 (i []byte) []int32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// ByteToInt64 does a unsafe cast from 'byte' to 'int64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func ByteToInt64 (i []byte) []int64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 8
  rethead.Len = ihead.Len / 8
  return ret
}

// ByteToFloat64 does a unsafe cast from 'byte' to 'float64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func ByteToFloat64 (i []byte) []float64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 8
  rethead.Len = ihead.Len / 8
  return ret
}

// ByteToUint8 does a unsafe cast from 'byte' to 'uint8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func ByteToUint8 (i []byte) []uint8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// ByteToUint32 does a unsafe cast from 'byte' to 'uint32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func ByteToUint32 (i []byte) []uint32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// ByteToUint64 does a unsafe cast from 'byte' to 'uint64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func ByteToUint64 (i []byte) []uint64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 8
  rethead.Len = ihead.Len / 8
  return ret
}

// ByteToInt8 does a unsafe cast from 'byte' to 'int8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func ByteToInt8 (i []byte) []int8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// ByteToString does a unsafe cast from 'byte' to 'string.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func ByteToString (i []byte) []string {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]string, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Float32ToInt64 does a unsafe cast from 'float32' to 'int64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float32ToInt64 (i []float32) []int64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Float32ToFloat64 does a unsafe cast from 'float32' to 'float64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float32ToFloat64 (i []float32) []float64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Float32ToUint8 does a unsafe cast from 'float32' to 'uint8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float32ToUint8 (i []float32) []uint8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Float32ToUint32 does a unsafe cast from 'float32' to 'uint32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float32ToUint32 (i []float32) []uint32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Float32ToUint64 does a unsafe cast from 'float32' to 'uint64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float32ToUint64 (i []float32) []uint64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Float32ToInt8 does a unsafe cast from 'float32' to 'int8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float32ToInt8 (i []float32) []int8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Float32ToInt16 does a unsafe cast from 'float32' to 'int16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float32ToInt16 (i []float32) []int16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Float32ToInt32 does a unsafe cast from 'float32' to 'int32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float32ToInt32 (i []float32) []int32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Float32ToString does a unsafe cast from 'float32' to 'string.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float32ToString (i []float32) []string {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]string, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Float32ToUint16 does a unsafe cast from 'float32' to 'uint16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float32ToUint16 (i []float32) []uint16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Float32ToByte does a unsafe cast from 'float32' to 'byte.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Float32ToByte (i []float32) []byte {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]byte, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 4
  rethead.Len = ihead.Len * 4
  return ret
}

// Uint16ToInt8 does a unsafe cast from 'uint16' to 'int8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint16ToInt8 (i []uint16) []int8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Uint16ToInt16 does a unsafe cast from 'uint16' to 'int16.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint16ToInt16 (i []uint16) []int16 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int16, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap 
  rethead.Len = ihead.Len 
  return ret
}

// Uint16ToInt32 does a unsafe cast from 'uint16' to 'int32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint16ToInt32 (i []uint16) []int32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Uint16ToInt64 does a unsafe cast from 'uint16' to 'int64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint16ToInt64 (i []uint16) []int64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]int64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// Uint16ToFloat64 does a unsafe cast from 'uint16' to 'float64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint16ToFloat64 (i []uint16) []float64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// Uint16ToUint8 does a unsafe cast from 'uint16' to 'uint8.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint16ToUint8 (i []uint16) []uint8 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint8, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Uint16ToUint32 does a unsafe cast from 'uint16' to 'uint32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint16ToUint32 (i []uint16) []uint32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}

// Uint16ToUint64 does a unsafe cast from 'uint16' to 'uint64.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint16ToUint64 (i []uint16) []uint64 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]uint64, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 4
  rethead.Len = ihead.Len / 4
  return ret
}

// Uint16ToString does a unsafe cast from 'uint16' to 'string.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint16ToString (i []uint16) []string {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]string, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Uint16ToByte does a unsafe cast from 'uint16' to 'byte.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint16ToByte (i []uint16) []byte {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]byte, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap * 2
  rethead.Len = ihead.Len * 2
  return ret
}

// Uint16ToFloat32 does a unsafe cast from 'uint16' to 'float32.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. 
// The caller is responsible for assuring size and offset. 
func Uint16ToFloat32 (i []uint16) []float32 {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  
  ret := make([]float32, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap / 2
  rethead.Len = ihead.Len / 2
  return ret
}
