package main

import (
	"fmt"
	"os"
	"strings"
	"text/template"
)

func main() {
	safe := false
	if len(os.Args) > 1 && os.Args[1] == "safe" {
		fmt.Println("Generating safe")
		safe = true
	}

	types := map[string]uint32{
		"uint8":   1,
		"uint16":  2,
		"uint32":  4,
		"uint64":  8,
		"int8":    1,
		"int16":   2,
		"int32":   4,
		"int64":   8,
		"byte":    1,
		"float32": 4,
		"float64": 8,
		"string":  1,
	}

	fi, err := os.OpenFile("sliceconv.go", os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0777)
	if err != nil {
		panic(err)
	}
	if safe {
		fi.WriteString("package sliceconvsafe")
	} else {
		fi.WriteString("package sliceconv")
	}

	fi.WriteString(`
import (
	"reflect"
	"unsafe"
)
`)

	tmpl, err := template.New("sliceconv").Parse(`
// {{.From}}To{{.To}} does a unsafe cast from '{{.from}}' to '{{.to}}.'
// The data referenced by the output may be GC'd, so keep the original.
// The caller is responsible for endianess conversion. {{if .safe}}
// The function will panic on misaligned inputs, or inputs with incorrect sizes. {{else}}
// The caller is responsible for assuring size and offset. {{end}}
func {{.From}}To{{.To}} (i []{{.from}}) []{{.to}} {
  ihead := (*reflect.SliceHeader)(unsafe.Pointer(&i))
  {{if .safe}}
  if ihead.Len%{{.sizeofTo}}!=0 {
    panic(fmt.Errorf("sliceconv: Invalid length %v for {{.to}}"))
  }
  if ihead.Cap%{{.sizeofTo}}!=0 {
    panic(fmt.Errorf("sliceconv: Invalid capacity %v for {{.to}}"))
  }
  var type {{.to}}
  if ihead.Data%unsafe.AlignOf(type)!=0{
    panic(fmt.Errorf("sliceconv: Invalid alignment for {{.to}}"))
  }
  {{end}}
  ret := make([]{{.to}}, 0, 0)
  rethead := (*reflect.SliceHeader)(unsafe.Pointer(&ret))
  rethead.Data = ihead.Data
  rethead.Cap = ihead.Cap {{.mult}}
  rethead.Len = ihead.Len {{.mult}}
  return ret
}
`)

	for f, fs := range types {
		for t, ts := range types {
			if f != t {
				mult := ""
				if fs > ts {
					mult = fmt.Sprintf("* %v", fs/ts)
				} else if ts > fs {
					mult = fmt.Sprintf("/ %v", ts/fs)
				}
				tmpl.Execute(fi, map[string]interface{}{
					"from":       f,
					"From":       Capsify(f),
					"sizeofFrom": fs,
					"to":         t,
					"To":         Capsify(t),
					"sizeofTo":   ts,
					"safe":       safe,
					"mult":       mult,
				})
			}
		}
	}
}

//Capsify turns `uint32` to `Uint32`
func Capsify(name string) string {
	return strings.ToUpper(name[0:1]) + name[1:]
}
